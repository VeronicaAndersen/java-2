# Java Assignment 2 - Data access with JDBC
Project with Spring Boot application in Java. First part creates scripts thats located in the folder Scripts. Second part is a Java application that reads & manipulating data with JDBC. Second part is like a re-make of iTunes called Chinnook. 

## Installation 

### Repository
The application is free to clone straight from gitlab. Type this into your selected git console to get the current main version: 
```
git clone git@gitlab.com:VeronicaAndersen/java-2.git
```

### IDE software
To run and use this repository you can use a software that you like. This one was build in IntelliJ IDEA.
The scripts was created in PostgreSQL with pgAdmin 4.


## Contributors
Huwaida Alhamdawee @Huwaida-al
Veronica Andersen @VeronicaAndersen
